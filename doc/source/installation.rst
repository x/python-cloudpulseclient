============
Installation
============

At the command line::

    $ pip install python-cloudpulseclient

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv python-cloudpulseclient
    $ pip install python-cloudpulseclient
