===============================
python-cloudpulseclient
===============================

A Python language binding for OpenStack CloudPulse.

* Free software: Apache license
* Documentation: https://wiki.openstack.org/wiki/Cloudpulse
* Source: http://git.openstack.org/cgit/openstack/python-cloudpulseclient
* Bugs: https://bugs.launchpad.net/python-cloudpulseclient

Features
--------

* TODO
